import { equal } from 'assert';
import hello from '../src/hello';

describe( 'hello test', () =>{
    it('return value function', () =>{
        equal(hello('Paulo'), 'Olá, Paulo!');
    })
})