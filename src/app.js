import Vue from 'vue/dist/vue.js'
import hello from './hello'


const app = new Vue ({
    el:'#app',
    data: {
        message: hello('Paulo César Filho')
    }
})